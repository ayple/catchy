from testhash import *
from colorama import *
import testtoken

def main():
    if test_hash() == "Matches":
        print(Fore.GREEN + "Hash comparing : Success" + Fore.RESET)
    else:
        print(Fore.RED + f"Hash comparing : Fail ({test_hash()})" + Fore.RESET)

    testtoken.tokentest()

    
main()