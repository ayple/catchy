import bcrypt
import sqlite3 
import os

def test_hash():

    log = "Emily"
    password = b"12345"
    #pash = bcrypt.hashpw(password, bcrypt.gensalt())

    con = sqlite3.connect(os.path.dirname(os.path.realpath(__file__)) + "/monsters.db")
    
    cur = con.cursor()

    #This is 100% sqli vulnerable but ill spend some time securing this later
    cur.execute(f'''SELECT * FROM logins WHERE user LIKE "{log}"''')

    c = cur.fetchall()

    if len(c) == 0:
        return "Username not found in database."

    cur.execute(f'''SELECT password FROM logins WHERE user LIKE "{log}"''')
    passhs = cur.fetchall()[0][0]

    if bcrypt.checkpw(password, passhs.encode()):
        return "Matches"