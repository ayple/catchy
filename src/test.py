from flask import Flask

app = Flask(__name__)

@app.route("/")
def index():
    return "index"

@app.route("/<path>:<path>")
def foo(path):
    return f"{path}"

app.run(port=4444, debug=True)