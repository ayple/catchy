"""
This file should contain the routes and stuff
for the admin control panel
"""

from flask import Blueprint 
from flask import request
from flask import render_template

import utils

admin = Blueprint(__name__, "admin", url_prefix="/admin")



# need to add a method for fetching stuff
# from the api
@admin.route("/")
def admin_page():
    users = [[i, "adam", True] for i in range(25)]

    utils.logger.log(f"Admin page visited by {utils.getip.getip(request)}")
    return render_template("admin.html", users=users)
