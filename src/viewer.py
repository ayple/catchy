from flask import Blueprint
from flask import render_template
from flask import request
import sqlite3
from utils import database
#from globals import STORAGE_DIR

import os

viewer = Blueprint(__name__, "viewer", url_prefix="/storage")


"""

The storage_items format should follow:
[0] -> "name of dir or file"
[1] -> "url path to dir or file"
[2] -> "is this a file or object?"

"""



@viewer.route("/")
def root():
    storage_items = list()

    ####Tested with curl only ####
    token = request.headers["token"]

    c = database.database_handler.get_user_by_token(token)


    if len(c) == 0:
        return "Authentication failed.", 401 #No owner = bogus token

    STORAGE_DIR = os.path.dirname(os.path.realpath(__file__)) + f"/../storage_root/{c[0][0]}" #Set directory to user's root
    ####Tested with curl only ####



    for file_name in os.listdir(STORAGE_DIR):
        file_path = os.path.join(STORAGE_DIR, file_name)
        url_path = f"/storage/{file_name}"
        is_file = False

        if os.path.isfile(file_path): 
            url_path = f"/api/download?path={file_name}"
            is_file = True


        storage_items.append([file_name, url_path, is_file])


    return render_template("treeview.html", 
        display_back=False, 
        storage=storage_items
    )




# need to make this work!
@viewer.route("/<path:path>")
def inner_dir(path):
    storage_items = []
    root_dir = os.path.join(STORAGE_DIR, path)

    if not os.path.exists(root_dir):
        return "This directory doesnt exist"

    try:
        for file_name in os.listdir(root_dir):
            file_url = f"{request.path}/{file_name}"
            is_file = False

            if os.path.isfile(os.path.join(root_dir, file_name)):
                file_url = f"/api/download?path={path}/{file_name}"
                is_file = True


            storage_items.append([file_name, file_url, is_file])
    except:
        return "Folder does not exist!"
    finally:
        return render_template("treeview.html",
            display_back=True,
            storage=storage_items
        )