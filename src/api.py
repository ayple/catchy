"""
I THOUGHT THE CODE WAS UNTIDY SO I WANTED TO CLEAN IT UP
"""

import bcrypt
import os
import utils.logger
import sqlite3
import json

from flask import *
# from auth import *
from utils import * 
from gentoken import gentoken
from utils.database import database_handler


api = Blueprint(__name__, 'api', url_prefix="/api")

#Define path to the storage root
here = os.path.dirname(os.path.realpath(__file__)) + "/../storage_root/" 

# we need to make sure path isnt null
@api.route("/download", methods=["GET"])
def download_file():

    #http://localhost:4444/api/download?path=a.txt
   
    j = request.args.get("path")
    
    #Double checking that nothing is being downloaded outside of storage_root 
    if ".." in j: return("No LFI for you you stupid troglodyte.")
    elif "storage_root" not in os.path.realpath(here + j): return("What were you trying to do here you scum?")


    utils.logger.log(f"File downloaded by {utils.getip.getip(request)}")
    return(send_file(here + j, as_attachment=True))


@api.route('/upload', methods=["POST"])
def upload_file():
    args = request.args
    f = request.files['file']
    
    dir = "" #Dir parameter will be used to specify the directory the file will be uploaded to
    if not args.get("dir"):
        pass
    elif ".." in args.get("dir"): #Security so no LFI can be done
        return("No LFI for you you degenerate baboon.")
    else:
        dir = args.get("dir")

    f.save(os.path.join(here, dir, f.filename)) 
    utils.logger.log(f"File uploaded by {utils.getip.getip(request)}")
    return "file uploaded successfully"


@api.route('/uploadpanel')#Test function, will be moved and replaced later
def upload_panel():

    return render_template('upload.html') 

#Only tested trough unit tests
@api.route("/login", methods=["POST"])
def login(): 
    data     = json.loads(request.data)
    log      = data["username"]
    password = data["password"]
    print([log, password])

    user = database_handler.get_user_by_username(log)
    
    if not user: return "Username not found in database.", 404
    passhs = user['password']
    
    
    try:
        if not bcrypt.checkpw(password.encode(), passhs.encode()): return "Password Wrong.", 401
        
        token = gentoken(log)
        database_handler.update_user_token(token, log)
        return token, 200
    except sqlite3.Error as e:
        return str(e), 500


#Only tested trough unit tests
@api.route("/register", methods=["POST"])
def register(): 
    log = request.headers["username"]
    password = request.headers["password"]
    token = gentoken(log)

    if not database_handler.create_new_user(log, password, token):
        return "Internal server error", 500
        

    # return token for storing
    utils.logger.log(f"User {log} has been registered {utils.getip.getip(request)}")
    return jsonify({'msg':'success', 'token':token}), 201 


@api.errorhandler(401)
def handle_401(e):
    utils.logger.log(f"Authentication failed for user {log} {utils.getip.getip(request)}")
    return "Incorrect password"

#@api.errorhandler(200)
#def handle_200(e):
    #utils.logger.log(f"Authentication success for user {log} {utils.getip.getip(request)}")
    #return "Logged in"
    
