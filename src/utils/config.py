import json
import os

class Config():
    def __init__(self, configfile):
        self.logfile = 0
        self.logverbose = 0
        self.db = 0
        self.configfile = configfile
    def unpack(self):
        with open(self.configfile, "r") as cf:
            os.chdir(os.path.dirname(os.path.realpath(__file__)) + "/../../")
            json_config = json.load(cf)
            self.logfile = os.path.realpath(json_config["logfile"])
            self.db = json_config["db"]
            self.logverbose = json_config["logverbosity"]


config = Config(os.path.dirname(os.path.realpath(__file__)) + "/../../conf/catchy.conf.json")
config.unpack()
