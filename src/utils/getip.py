

def getip(request):
    return request.environ['REMOTE_ADDR'] if not request.environ.get('HTTP_X_FORWARDED_FOR') else request.environ.get('HTTP_X_FORWARDED_FOR')