from utils.config import config
from datetime import *


def log(text):
    f = open(config.logfile, "a")
    f.write(f"""{datetime.now()} {text} \n""")
    f.close()
