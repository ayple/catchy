import random
import string
import base64


def tolst(string):
    lists=[]
    lists[:0] = string
    return lists

def gentoken(user:str, l=45):
    pool = string.ascii_letters + string.octdigits
    salt = "".join(random.choice(pool) for _ in range(l))
    token = base64.b64encode(bytes(user, "utf-8"))
    
    return salt + token.decode()

def test():
    for i in range(20):
        print(gentoken("gsgsbvdsgs"))

if __name__ == '__main__':
    test()