import sqlite3
import os

def purge_all_tokens():
    con = sqlite3.connect(os.path.dirname(os.path.realpath(__file__)) + "/../../data/monsters.db") 
    cur = con.cursor()
    cur.execute("""UPDATE logins SET token = NULL""")
    con.commit()