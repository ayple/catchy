import sqlite3
import os
import bcrypt


global database_handler


class _DatabaseHandler(object):
    num_of_instances = 0
    
    
    def __init__(self):
        if self.num_of_instances >= 1:
            raise Exception("there is already a _DatabaseHandler defined")
        
        self.con = sqlite3.connect(os.path.dirname(os.path.realpath(__file__)) + "/../../data/monsters.db", check_same_thread=False)
        self.cur = self.con.cursor()
        
        
        self.num_of_instances += 1
    
    
    def get_user_by_username(self, username):
        self.cur.execute(f"SELECT * FROM logins WHERE user = ?", (username, ))
        user = self.cur.fetchall()
        
        if len(user) > 0: return dict(
            {
                'username': user[0][0],
                'password': user[0][1],
                'token':    user[0][2]
            }
        )
        
        return None
    
    
    def user_exists(self, username) -> bool:
        if len(self.get_user_by_username(username) > 0): return True
        return False
    
    
    def update_user_token(self, token, username) -> bool:
        try:
            self.cur.execute("""UPDATE logins SET token = ? WHERE user = ?""", (token, username))
            self.con.commit()
            return True
        except sqlite3.Error as e:
            print(str(e))
            return False
        
    
    def create_new_user(self, username, password, token) -> bool:
        try:
            pass_hash = bcrypt.hashpw(password.encode(), bcrypt.gensalt())
            self.cur.execute(f'''INSERT INTO logins VALUES (?, ?, ?)''', 
                (
                    username, 
                    pass_hash.decode("utf-8"), 
                    token
                )
            )
            
            self.con.commit()
            return True
        except sqlite3.Error as e:
            print(str(e))
            return False
    def get_user_by_token(self, token):

        con = sqlite3.connect(os.path.dirname(os.path.realpath(__file__)) + "/../../data/monsters.db") 
        cur = con.cursor()

        cur.execute("SELECT user FROM logins WHERE token LIKE ?", (token,)) #Get owner of the token
        return cur.fetchall()
            
        
    
    
    def __con__(self): return self.con
    def __cur__(self): return self.cur    
    def __del__(self): self.con.close()

database_handler = _DatabaseHandler()

#[('Emily', '$2a$12$AQiaM4OzhDGH50nd6esp2eOUl1WXF4gCSEtKil2xmV3jZ6CslaPAy', 'f6txc55nkPJrK7Kp17IaomYcJiEwppT30K5VY5Y4kftbMRW1pbHk=')]

#print(database_handler.get_user_by_username("Emily"))
