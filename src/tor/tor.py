from flask import Flask


tor = Flask(__name__)

@tor.route("/")
def tor_index():
    pass

@tor.route("/login")
def tor_login():
    pass

@tor.route("/admin")
def tor_admin():
    pass