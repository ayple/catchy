from flask import *
import os
# from auth import *
from utils import * 
import bcrypt

from gentoken import gentoken
import utils.logger

api = Blueprint(__name__, 'api', url_prefix="/api")
here = os.path.dirname(os.path.realpath(__file__)) + "/../storage_root/" #Define path to the storage root


# the route for this would be http://localhost:4444/api/foo
@api.route("/foo")
def foo():
    pass

# @api.route("/download", methods=["GET"])
# def download():
#     return redirect(url_for('index'))

# we need to make sure path isnt null
@api.route("/download", methods=["GET"])
def download_file():

    #http://localhost:4444/api/download?path=a.txt
   
    args = request.args
    j = args.get("path")
    
	# this is smart well done
    if ".." in j: 
        return("No LFI for you you stupid troglodyte.")
    elif "storage_root" not in os.path.realpath(here + j): #Double checking that nothing is being downloaded outside of storage_root.
        return("What were you trying to do here you scum?")


    utils.logger.log(f"File downloaded by {utils.getip.getip(request)}")
    return(send_file(here + j, as_attachment=True))


@api.route('/upload', methods=["POST"])
def upload_file():
    args = request.args
    f = request.files['file']
    
    dir = "" #Dir parameter will be used to specify the directory the file will be uploaded to
    if not args.get("dir"):
        pass
    elif ".." in args.get("dir"): #Security so no LFI can be done
        return("No LFI for you you degenerate baboon.")
    else:
        dir = args.get("dir")

    f.save(os.path.join(here, dir, f.filename)) 
    utils.logger.log(f"File uploaded by {utils.getip.getip(request)}")
    return "file uploaded successfully"


@api.route('/uploadpanel')#Test function, will be moved and replaced later
def upload_panel():

    return render_template('upload.html') 


@api.route("/login")
def login(): #Only tested trough unit tests.
    log = request.headers["login"]
    password = request.headers["pass"]
    con = sqlite3.connect(os.path.dirname(os.path.realpath(__file__)) + "/../data/monsters.db")
    cur = con.cursor()

    cur.execute(f"SELECT * FROM logins WHERE user = ?", (log, )) #Check if user exists
    c = cur.fetchall()

    if len(c) == 0:
        return "Username not found in database.", 404

    
    cur.execute(f"SELECT password FROM logins WHERE user = ?", (log, )) #Get password hash for comparison
    passhs = cur.fetchall()[0][0]
    print(passhs)

    if bcrypt.checkpw(password.encode(), passhs.encode()):
        token = gentoken(log)
        cur.execute("""UPDATE logins SET token = ? WHERE user = ?""", (token, log))
        con.commit()
        utils.logger.log(f"Authentication success for user {log} {utils.getip.getip(request)}")
        return token, 200
    else:
        utils.logger.log(f"Authentication failed for user {log} {utils.getip.getip(request)}")
        return "Password Wrong.", 401


@api.route("/register", methods=["POST"])
def register(): #Only tested trough unit tests.
    log = request.headers["username"]
    password = request.headers["password"]
    con = sqlite3.connect(os.path.dirname(os.path.realpath(__file__)) + "/../data/monsters.db")
    token = gentoken(log)
    cur = con.cursor()

    cur.execute(f'''INSERT INTO logins VALUES ( ?, ?, ?)''', (log, bcrypt.hashpw(password.encode(), bcrypt.gensalt()).decode(), token)) #Create new user 
    con.commit()

    utils.logger.log(f"User {log} has been registered {utils.getip.getip(request)}")


    return f"Success, token: {token}", 201 #Return token for storing
