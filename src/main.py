

from flask import *
import os
#from waitress import serve
#FOR PRODUCTION ONLY!!

# blueprints
from viewer import viewer
from api import api
import utils.logger
import utils.getip
import utils.purgetokens
from admin import admin

app = Flask(__name__)
app.register_blueprint(api)
app.register_blueprint(viewer)
app.register_blueprint(admin)

@app.route("/index")
@app.route("/")
def index():

    utils.logger.log(f"Index visited by {utils.getip.getip(request)}")
    return render_template("index.html")

@app.route("/login", methods=["GET"])
def login():
    return render_template("login.html")

@app.route("/signup", methods=["GET"])
def signup():
    return render_template("signup.html")

@app.errorhandler(404)
def handle_404(e):
    return "The target resource is not fount!"

if __name__ == '__main__':
    utils.logger.log("Catchy started, initializing Flask app.")
    #utils.purgetokens.purge_all_tokens() Production only!!!
    app.run(port=4444, debug=True) #Flask reloader makes double logs in debug mode, its fine as this doesnt happen in production.
    #serve(app, host="127.0.0.1", port=4444)
    #FOR PRODUCTION ONLY!!
    utils.logger.log("Catchy stopped.")

